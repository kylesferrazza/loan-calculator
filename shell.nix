{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "loans";
  buildInputs = with pkgs; [
    nodePackages.yarn nodejs
  ];
}
